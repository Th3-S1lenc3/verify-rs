// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use clap::Parser;
use colored::*;
use path_absolutize::*;

use hex;
use sha2::digest::DynDigest;
use std::fs::File;
use std::io::{self, BufReader, Read};
use std::path::Path;

use md5::Md5;
use sha1::Sha1;
use sha2::{Sha224, Sha256, Sha384, Sha512};

/// A command line program to verify a given file against a given hash.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// File to verify
    #[clap(short, long)]
    file: String,

    /// Hash to compare against
    #[clap(short, long)]
    hash: String,

    /// Be Verbose
    #[clap(short, long)]
    verbose: bool,
}

fn hash_file<P: AsRef<Path>>(hasher: &mut dyn DynDigest, path: P) -> io::Result<String> {
    let file = File::open(&path)?;
    let mut reader = BufReader::new(file);

    let mut buffer = [0; 1024];
    loop {
        let count = reader.read(&mut buffer)?;

        if count == 0 {
            break;
        }

        hasher.update(&buffer[..count]);
    }

    Ok(hex::encode(hasher.finalize_reset()))
}

fn main() {
    let args = Args::parse();

    let file_path_rel = Path::new(&args.file);
    let file_path = file_path_rel.absolutize().unwrap();
    let file = file_path.to_str().unwrap();

    let hash_result: io::Result<String> = match args.hash.len() {
        32 => hash_file(&mut Md5::default(), &file),
        40 => hash_file(&mut Sha1::default(), &file),
        56 => hash_file(&mut Sha224::default(), &file),
        64 => hash_file(&mut Sha256::default(), &file),
        96 => hash_file(&mut Sha384::default(), &file),
        128 => hash_file(&mut Sha512::default(), &file),
        _ => return println!("{}", "Invalid Hash Type.".bright_red()),
    };

    let computed_hash: String = match hash_result {
        Ok(r) => r,
        Err(e) => return println!("{}", format!("Error: {}", e).bright_red()),
    };

    if args.verbose == true {
        println!("File: {}", file);
        println!("Provided Hash: {}", args.hash);
        println!("Computed Hash: {}", computed_hash);
    }

    if computed_hash == args.hash {
        println!("{}", "Hashes match. File Valid.".green());
    } else {
        println!("{}", "Hashes do not match. File Invalid.".bright_red());
    }
}
